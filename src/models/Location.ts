import {
  Entity,
  Column,
  OneToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

import HumanAddress from './HumanAddress';

@Entity('locations')
class Location {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('numeric')
  longitude: number;

  @Column('numeric')
  latitude: number;

  @Column()
  needs_recoding: boolean;

  @Column()
  human_address_id: number;

  @OneToOne(() => HumanAddress)
  @JoinColumn({ name: 'human_address_id' })
  human_address: HumanAddress;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Location;
