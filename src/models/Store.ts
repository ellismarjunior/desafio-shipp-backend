import {
  Entity,
  Column,
  OneToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

import Location from './Location';

@Entity('stores')
class Store {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  country: string;

  @Column('numeric')
  license_number: number;

  @Column()
  operation_type: string;

  @Column()
  establishment_type: string;

  @Column()
  entity_name: string;

  @Column()
  dba_name: string;

  @Column('numeric')
  street_number: number;

  @Column()
  street_name: string;

  @Column()
  address_line2: string;

  @Column()
  address_line3: string;

  @Column()
  city: string;

  @Column()
  state: string;

  @Column('numeric')
  zipcode: number;

  @Column()
  square_footage: string;

  @Column()
  location_id: number;

  @OneToOne(() => Location)
  @JoinColumn({ name: 'location_id' })
  location: Location;

  distance: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Store;
