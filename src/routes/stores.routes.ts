import { Router } from 'express';
import multer from 'multer';

import uploadConfig from '../config/upload';

import ImportCSVService from '../services/ImportCSVService';
import ListStoreService from '../services/ListStoreService';

import existLatitudeLongitudeInBody from '../middlewares/existLatitudeLongitudeInBody';

const storesRouter = Router();
const upload = multer(uploadConfig);

storesRouter.post(
  '/upload',
  upload.single('file'),
  async (request, response) => {
    const importCSVService = new ImportCSVService();

    await importCSVService.execute(request.file.path);

    return response.status(201);
  },
);

storesRouter.get(
  '/V1/stores',
  existLatitudeLongitudeInBody,
  async (request, response) => {
    const { latitude, longitude } = request.body;
    const listStoreService = new ListStoreService();

    const stores = await listStoreService.execute({ latitude, longitude });
    return response.json(stores);
  },
);

export default storesRouter;
