class OrderStoreByDistance {
  public execute(stores: Array<any>): any {
    return stores.sort((storeA, storeB) => {
      if (storeA.distance > storeB.distance) {
        return 1;
      }

      if (storeA.distance < storeB.distance) {
        return -1;
      }

      return 0;
    });
  }
}

export default OrderStoreByDistance;
