import { getRepository } from 'typeorm';
import { getDistance, convertDistance, toDecimal } from 'geolib';
import Store from '../models/Store';

import OrderStoreByDistance from './OrderStoreByDistance';

interface Request {
  latitude: string;
  longitude: string;
}

class ListStoreService {
  public async execute({ latitude, longitude }: Request): Promise<any> {
    const orderStoreByDistance = new OrderStoreByDistance();

    const storeRepository = getRepository(Store);

    const stores = await storeRepository.find({
      join: {
        alias: 'store',
        leftJoinAndSelect: {
          location: 'store.location',
        },
      },
    });

    const storesWithDistance = stores.map(store => {
      if (
        store.location &&
        store.location.latitude &&
        store.location.longitude
      ) {
        const geoDistance = getDistance(
          { latitude, longitude },
          {
            latitude: toDecimal(store.location.latitude),
            longitude: toDecimal(store.location.longitude),
          },
        );
        const distanceFomartted = convertDistance(geoDistance, 'km').toFixed(1);
        return { ...store, distance: distanceFomartted };
      }

      return store;
    });

    const storeFiltered = storesWithDistance.filter(
      store => store.distance < 6.5,
    );

    const storesOrderByDistance = orderStoreByDistance.execute(storeFiltered);

    return storesOrderByDistance;
  }
}

export default ListStoreService;
