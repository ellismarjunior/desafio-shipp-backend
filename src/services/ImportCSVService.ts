import { getRepository } from 'typeorm';
import csvParse from 'csv-parse';
import fs from 'fs';

import HumanAddress from '../models/HumanAddress';
import Location from '../models/Location';
import Store from '../models/Store';

import AppError from '../errors/AppError';

interface IStore {
  country: string;

  license_number: number;

  operation_type: string;

  establishment_type: string;

  entity_name: string;

  dba_name: string;

  street_number: number;

  street_name: string;

  address_line2: string;

  address_line3: string;

  city: string;

  state: string;

  zipcode: number;

  square_footage: string;

  locationO: string;
}

interface ILocation {
  longitude: string;

  latitude: string;

  needs_recoding: boolean;

  human_address: string;
}

interface IHumanAddress {
  address: string;

  city: string;

  state: string;

  zip: number;
}

class ImportCSVService {
  public async execute(filePath: string): Promise<void> {
    const human_addressRepository = getRepository(HumanAddress);
    const storeRepository = getRepository(Store);
    const locationRepository = getRepository(Location);
    const readCSVStream = fs.createReadStream(filePath);

    const parseStream = csvParse({
      from_line: 2,
      ltrim: true,
      rtrim: true,
    });

    const parseCSV = readCSVStream.pipe(parseStream);

    parseCSV.on('data', async line => {
      try {
        const [
          country,
          license_number,
          operation_type,
          establishment_type,
          entity_name,
          dba_name,
          street_number,
          street_name,
          address_line2,
          address_line3,
          city,
          state,
          zipcode,
          square_footage,
          location,
        ] = line;

        if (location) {
          const replaceTrailing = location.replace(/'/g, '"');
          const replaceOpenKeys = replaceTrailing.replace('"{', '{');
          const replaceCloseKeys = replaceOpenKeys.replace('}"', '}');
          const replaceFalseTofalse = replaceCloseKeys.replace(
            'False',
            'false',
          );
          const locationO = JSON.parse(replaceFalseTofalse);
          const {
            longitude,
            needs_recoding,
            human_address,
            latitude,
          } = locationO;

          const { address, city: cityA, state: stateA, zip } = human_address;

          const newHumanAddress = human_addressRepository.create({
            address,
            city: cityA,
            state: stateA,
            zip,
          });
          await human_addressRepository.save(newHumanAddress);

          const newLocation = locationRepository.create({
            latitude,
            longitude,
            needs_recoding,
            human_address_id: newHumanAddress.id,
          });
          await locationRepository.save(newLocation);

          const newStore = storeRepository.create({
            country,
            license_number,
            operation_type,
            establishment_type,
            entity_name,
            dba_name,
            street_number,
            street_name,
            address_line2,
            address_line3,
            city,
            state,
            zipcode,
            square_footage,
            location_id: newLocation.id,
          });

          await storeRepository.save(newStore);
        }
      } catch (error) {
        throw new AppError('Error to load file');
      }
    });

    await new Promise(resolve => parseCSV.on('end', resolve));

    fs.promises.unlink(filePath);
  }
}

export default ImportCSVService;
