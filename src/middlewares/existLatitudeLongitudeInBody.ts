import { Request, Response, NextFunction } from 'express';

import AppError from '../errors/AppError';

export default function existLatitudeLongitudeInBody(
  request: Request,
  reponse: Response,
  next: NextFunction,
): void {
  const { latitude, longitude } = request.body;
  if (!latitude || !longitude) {
    throw new AppError('Latitude and longitude are required');
  }

  return next();
}
