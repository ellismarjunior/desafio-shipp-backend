import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export default class createStores1594521322129 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'stores',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'country',
            type: 'varchar',
          },
          {
            name: 'license_number',
            type: 'numeric',
          },
          {
            name: 'operation_type',
            type: 'varchar',
          },
          {
            name: 'establishment_type',
            type: 'varchar',
          },
          {
            name: 'entity_name',
            type: 'varchar',
          },
          {
            name: 'dba_name',
            type: 'varchar',
          },
          {
            name: 'street_number',
            type: 'numeric',
          },
          {
            name: 'street_name',
            type: 'varchar',
          },
          {
            name: 'address_line2',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'address_line3',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'city',
            type: 'varchar',
          },
          {
            name: 'state',
            type: 'varchar',
          },
          {
            name: 'zipcode',
            type: 'numeric',
          },
          {
            name: 'square_footage',
            type: 'varchar',
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('stores');
  }
}
