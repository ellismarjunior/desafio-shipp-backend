import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
} from 'typeorm';

export default class AddHumanAddressIdToLocation1594549282215
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'locations',
      new TableColumn({
        name: 'human_address_id',
        type: 'integer',
        isNullable: true,
      }),
    );

    await queryRunner.createForeignKey(
      'locations',
      new TableForeignKey({
        name: 'LocationHumanAddress',
        columnNames: ['human_address_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'human_addresses',
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('locations', 'LocationHumanAddress');
    await queryRunner.dropColumn('locations', 'human_address_id');
  }
}
