import { EntityRepository, Repository } from 'typeorm';

import HumanAddress from '../models/HumanAddress';

@EntityRepository(HumanAddress)
class HumanAddressesRepository extends Repository<HumanAddress> {}

export default HumanAddressesRepository;
